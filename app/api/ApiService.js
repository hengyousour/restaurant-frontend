import axios from "axios";
let localStorage = require("nativescript-localstorage");
var ip = localStorage.getItem("ip");

export const APIService = axios.create({
    //60 sec timeout
    timeout: 60000,
    baseURL: "http://" + ip
});