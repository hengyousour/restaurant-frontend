//note : method format date for monogoDB//
import wrapCurrentTime from "../utils/wrap-current-time";
export default function formatDate(date) {
  let splitDate = date.split("/");
  let day = splitDate[0];
  let month = splitDate[1];
  let year = splitDate[2];
  return wrapCurrentTime(new Date(year, month - 1, day));
}