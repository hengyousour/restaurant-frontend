import { TNSFancyAlert, TNSFancyAlertButton } from "nativescript-fancyalert";

const Alert = {
  Success(msg) {
    return TNSFancyAlert.showColorDialog(
      "Success",
      msg,
      undefined,
      undefined,
      "#27AE60"
    );
  },
  Error(msg) {
    return TNSFancyAlert.showColorDialog(
      "Error",
      msg,
      undefined,
      undefined,
      "#E74C3C"
    );
  },
  Info(msg) {
    return TNSFancyAlert.showColorDialog(
      "Infomation",
      msg,
      undefined,
      undefined,
      "#3498DB"
    );
  },
  Warning(msg) {
    return TNSFancyAlert.showColorDialog(
      "Warning",
      msg,
      undefined,
      undefined,
      "#F39C12"
    );
  },
}

export default Alert;
