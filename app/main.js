import Vue from "nativescript-vue";
import store from "./store";

// Prints Vue logs when --env.production is *NOT* set while building
// Vue.config.silent = TNS_ENV === "production"; true;
Vue.config.silent = true;
import LogIn from "./views/LogIn";

import RadListView from "nativescript-ui-listview/vue";
Vue.use(RadListView);

Vue.registerElement("Rippler", () => require("nativescript-ripple").Ripple);

// Filter format date on template
import LocalFilters from "./plugins/filters";
Vue.use(LocalFilters);

// import { isIOS, isAndroid } from "platform";
// import * as app from 'tns-core-modules/application';
// import * as platform from "platform"

// if (app.android && platform.device.sdkVersion <= "21") {
//     app.android.on("activityStarted", () => {
//         const View = android.view.View;
//         const window = app.android.startActivity.getWindow();
//         window.setStatusBarColor(0x000000);

//         const decorView = window.getDecorView();
//         decorView.setSystemUiVisibility(
//             View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
//             View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION |
//             View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN |
//             View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
//     });
// }

// // Make the iOS status bar transparent with white text.
// if (app.ios) {
//     app.on("launch", () => {
//         utils.ios.getter(UIApplication, UIApplication.sharedApplication).statusBarStyle = UIStatusBarStyle.LightContent;
//     });
// }

new Vue({
    store,
    render: h => h("frame", [h(LogIn)])
}).$start();