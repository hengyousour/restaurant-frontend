import _ from 'lodash'
import moment from 'moment'
import numeral from 'numeral'

const Filters = {
    install(Vue, options) {
        Vue.mixin({
            filters: {
                date: formatDate,
                dateTime: formatDateTime,
                currency: formatCurrency
            },
        })
    },
}

export default Filters

/**
 * Functions
 */
function formatDate(value, format) {
    let formatDate =
        format || 'DD/MM/YYYY'
    return moment(value).format(formatDate)
}

function formatDateTime(value, format) {
    let formatDateTime = format || 'DD/MM/YYYY H:mm:ss'
    return moment(value).format(formatDateTime)
}

function formatCurrency(value, format, currency) {
    let formatCurrency = format || '0,0.00'
    let baseCurrency;
    if (currency === "USD") {
        baseCurrency = "$";
    } else if (currency === "KHR") {
        baseCurrency = "៛";
    } else {
        baseCurrency = "B";
    }

    return numeral(value).format(formatCurrency) + ` ${baseCurrency}`
}